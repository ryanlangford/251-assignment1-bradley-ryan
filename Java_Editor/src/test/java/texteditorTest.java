import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import javax.swing.text.BadLocationException;
import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;


class texteditorTest {




    @Test
    public void test_save_file() {
        texteditor e = new texteditor();
        e.textarea.setText("Test_script");
        File test_file = new File("Test_file");
        e.file_ext_chooser(e.textarea.getText(), test_file);
        File checkfile = new File("Test_file.txt");
        assert(checkfile.exists());




    }

    @Test
    public void test_open_file(){
        texteditor texteditor = new texteditor();
        File open_file = new File("Test_file.txt");
        texteditor.open(open_file);
        String compare_string = texteditor.textarea.getText();
        assertEquals(compare_string,"Test_script\n");


    }

    @Test
    public void test_search() throws BadLocationException {
        texteditor e = new texteditor();
        RSyntaxTextArea textarea = new RSyntaxTextArea();
        e.add(textarea);
        textarea.setText("Test_script");
        assert(texteditor.highLight(textarea, "Test"));

    }


    @AfterAll
    public static void doYourOneTimeTeardown() {
        File checkfile = new File("Test_file.txt");
        checkfile.delete();

    }
}