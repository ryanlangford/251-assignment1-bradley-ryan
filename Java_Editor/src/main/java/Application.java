import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.odf.OpenDocumentParser;
import org.apache.tika.sax.BodyContentHandler;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;


class texteditor extends JFrame implements ActionListener {

    // Text component
    RSyntaxTextArea textarea;

    // Frame
    JFrame framebox;

    // Constructor
    texteditor() {
        // Create a frame
        framebox = new JFrame("Text Editor");
        JPanel cp = new JPanel(new BorderLayout());
        textarea = new RSyntaxTextArea(20, 60);
        //Creates Scroll Bar
        RTextScrollPane sp = new RTextScrollPane(textarea);
        //Adds scroll bar to JPanel
        cp.add(sp);
        setContentPane(cp);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);

        try {
            // Set metl look and feel
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");

            // Set theme to ocean
            MetalLookAndFeel.setCurrentTheme(new OceanTheme());
        } catch (Exception e) {
        }


        // Create a menubar
        JMenuBar menubar = new JMenuBar();

        // Create menu
        JMenu file_menu = new JMenu("File");

        // Create  file menu items
        JMenuItem menu_new = new JMenuItem("New");
        JMenuItem menu_open = new JMenuItem("Open");
        JMenuItem menu_save = new JMenuItem("Save");
        JMenuItem menu_pdf = new JMenuItem("Save to PDF");
        JMenuItem menu_print = new JMenuItem("Print");

        // Looks for presses
        menu_new.addActionListener(this);
        menu_open.addActionListener(this);
        menu_save.addActionListener(this);
        menu_print.addActionListener(this);
        menu_pdf.addActionListener(this);


        file_menu.add(menu_new);
        file_menu.add(menu_open);
        file_menu.add(menu_save);
        file_menu.add(menu_print);
        file_menu.add(menu_pdf);

        // Create help menu
        JMenu help_menu = new JMenu("Help");

        // Create help menu items
        JMenuItem menu_about = new JMenuItem("About");

        //Looks for Button Presses
        menu_about.addActionListener(this);

        help_menu.add(menu_about);

        // Create edit menu
        JMenu edit_menu = new JMenu("Edit");

        // Create edit  menu items
        JMenuItem menu_cut = new JMenuItem("Cut");
        JMenuItem menu_copy = new JMenuItem("Copy");
        JMenuItem menu_paste = new JMenuItem("Paste");
        JMenuItem menu_date = new JMenuItem("Date");
        JMenuItem menu_search = new JMenuItem("Search");

        // Looks for button presses
        menu_cut.addActionListener(this);
        menu_copy.addActionListener(this);
        menu_paste.addActionListener(this);
        menu_date.addActionListener(this);
        menu_search.addActionListener(this);

        edit_menu.add(menu_cut);
        edit_menu.add(menu_copy);
        edit_menu.add(menu_paste);
        edit_menu.add(menu_date);
        edit_menu.add(menu_search);


        JMenuItem close_menu = new JMenuItem("Close");

        close_menu.addActionListener(this);


        //view menu and .odt viewer
        JMenu view_menu = new JMenu("View");
        JMenuItem menu_ODT = new JMenuItem("View .ODT File");
        menu_ODT.addActionListener(this);
        view_menu.add(menu_ODT);

        //Syntax Menu and selector
        JMenu syntax_menu = new JMenu("Syntax Selector");

        JMenuItem menu_python = new JMenuItem("Python");
        menu_python.addActionListener(this);

        JMenuItem menu_java = new JMenuItem("Java");
        menu_java.addActionListener(this);

        JMenuItem menu_html = new JMenuItem("HMTL");
        menu_html.addActionListener(this);

        JMenuItem menu_none = new JMenuItem("Clear");
        menu_none.addActionListener(this);

        syntax_menu.add(menu_python);
        syntax_menu.add(menu_java);
        syntax_menu.add(menu_html);
        syntax_menu.add(menu_none);

        menubar.add(file_menu);
        menubar.add(edit_menu);
        menubar.add(help_menu);
        menubar.add(view_menu);
        menubar.add(syntax_menu);
        menubar.add(close_menu);


        framebox.setJMenuBar(menubar);
        framebox.add(cp);
        framebox.setSize(750, 750);
        framebox.show();
    }

    // If button pressed -> do something
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();

        if (s.equals("Cut")) {
            textarea.cut();
        } else if (s.equals("Copy")) {
            textarea.copy();
        } else if (s.equals("Paste")) {
            textarea.paste();

        } else if (s.equals("New")) {
            textarea.setText("");
        } else if (s.equals("Close")) {
            framebox.setVisible(false);
            dispose();
        } else if (s.equals("View .ODT File")) {
            try {
                parse_odt();
            } catch (TikaException | SAXException | IOException e1) {
                e1.printStackTrace();
            }
        }

        // Gets system time, copies it to the clipboard then pastes it
        else if (s.equals("Date")) {
            paste_date();
        } else if (s.equals("Search")) {
            String search_string = JOptionPane.showInputDialog("Enter search term");
            highLight(textarea, search_string);
        } else if (s.equals("About")) {
            JOptionPane.showMessageDialog(null, "Check out this awesome text editor. \n Created by Bradley Grapes and Ryan Langord.");
        } else if (s.equals("Print")) {
            PrintActionPreformed(textarea);
        } else if (s.equals("Open")) {


            JFileChooser fs = new JFileChooser(new File("c:\\"));
            fs.setDialogTitle("Open");
            int result = fs.showOpenDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                File fi = fs.getSelectedFile();
                open(fi);

            }
        } else if (s.equals("Save")) {
            JFileChooser fs = new JFileChooser(new File("c:\\"));
            fs.setDialogTitle("Save a File");
            fs.setFileFilter(new FileTypeFilter(".txt", "Text File"));
            int result = fs.showSaveDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                String content = textarea.getText();
                File fi = fs.getSelectedFile();
                file_ext_chooser(content, fi);


            }
        } else if (s.equals("Save to PDF")) {
            PrintFrameToPDF(textarea);

            //Sets syntax to Java
        } else if (s.equals("Java")) {
            textarea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
            //Sets syntax to Python
        } else if (s.equals("Python")) {
            textarea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
            //Sets syntax to HTML
        } else if (s.equals("HTML")) {
            textarea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);
            //Sets syntax to Clear
        } else if (s.equals("Clear")) {
            textarea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
        }

    }


    public void PrintActionPreformed(JTextArea evt) {
        try {
            boolean complete = textarea.print();
            if (complete) {
                JOptionPane.showMessageDialog(null, "Done Printing", "Information",
                        JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Printing!", "Printer",
                        JOptionPane.ERROR_MESSAGE);
            }
        } catch (PrinterException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }


    public void paste_date() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
        String date = dateFormat.format(new Date());

        try {
            textarea.getDocument().insertString(0, date + "\n" + "\n", null);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }


    }

    public class FileTypeFilter extends FileFilter {

        private final String extension;
        private final String description;

        public FileTypeFilter(String extension, String description) {
            this.description = extension;
            this.extension = description;
        }

        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            }
            return file.getName().endsWith(extension);
        }

        @Override
        public String getDescription() {
            return description + String.format(" (*%s)", extension);
        }
    }


    public void parse_odt() throws TikaException, SAXException, IOException {
        JFileChooser fs = new JFileChooser(new File("c:\\"));
        fs.setDialogTitle("Open");
        int result = fs.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            try {
                File fi = fs.getSelectedFile();


                //detecting the file type
                BodyContentHandler handler = new BodyContentHandler();
                Metadata metadata = new Metadata();
                FileInputStream inputstream = new FileInputStream(fi);
                ParseContext pcontext = new ParseContext();

                //Open Document Parser
                OpenDocumentParser openOfficeParser = new OpenDocumentParser();
                openOfficeParser.parse(inputstream, handler, metadata, pcontext);
                System.out.println("Contents of the document:" + handler.toString());
                textarea.setText(handler.toString());
                System.out.println("Metadata of the document:");
                String[] metadataNames = metadata.names();

                for (String name : metadataNames) {
                    System.out.println(name + " :  " + metadata.get(name));
                }
            } catch (Exception e) {

            }
        }
    }


    // Creates highlights around all occurrences of pattern in textComp
    public static boolean highLight(RSyntaxTextArea textComp, String pattern) {
        // First remove all old highlights
        removeHighlights(textComp);

        try {

            Highlighter hiLite = textComp.getHighlighter();
            javax.swing.text.Document doc = textComp.getDocument();
            String text = doc.getText(0, doc.getLength());
            for (int i = 0; i < pattern.length(); i++) {
                int pos = 0;
                // Search for pattern
                while ((pos = text.toUpperCase().indexOf(pattern.toUpperCase(), pos)) >= 0) {
                    hiLite.addHighlight(pos, pos + pattern.length(), myHighlighter);
                    pos += pattern.length();
                    return true;
                }
            }
            return false;
        } catch (BadLocationException e) {
            return false;
        }

    }

    //Removes Highlighted words
    public static void removeHighlights(JTextComponent textComp) {

        Highlighter hiLite = textComp.getHighlighter();

        Highlighter.Highlight[] hiLites = hiLite.getHighlights();

        for (int i = 0; i < hiLites.length; i++) {

            if (hiLites[i].getPainter() instanceof MyHighlightPainter) {

                hiLite.removeHighlight(hiLites[i]);
            }
        }
    }

    // An instance of the private subclass of the default highlight painter
    static Highlighter.HighlightPainter myHighlighter = new MyHighlightPainter(Color.YELLOW);

    // A class of the default highlight painter
    private static class MyHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {

        public MyHighlightPainter(Color color) {

            super(color);

        }
    }


    public void file_ext_chooser(String content, File file) {
        if (!file.getName().endsWith(".txt")) {
            String oldPath = file.getPath();
            String new_name = oldPath.concat(".txt");
            File new_file_name = new File(new_name);

            save(content, new_file_name);

        } else {
            save(content, file);
        }


    }

    public void save(String content, File fi) {

        try {
            FileWriter fw = new FileWriter(fi.getPath());
            fw.write(content);
            fw.flush();
            fw.close();
        } catch (Exception e2) {
            JOptionPane.showMessageDialog(null, e2.getMessage());
        }

    }

    public void open(File fi) {
        try {

            BufferedReader br = new BufferedReader(new FileReader(fi.getPath()));
            String line;
            String s1 = "";
            while ((line = br.readLine()) != null) {
                s1 += line + "\n";
            }
            textarea.setText(s1);
            br.close();
        } catch (Exception e2) {
            JOptionPane.showMessageDialog(null, e2.getMessage());
        }
    }

    public void PrintFrameToPDF(RSyntaxTextArea textComp) {
        JFileChooser fs = new JFileChooser(new File("c:\\"));
        fs.setDialogTitle("Save a File as PDF");

        int result = fs.showSaveDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            String content = textComp.getText();
            File fi = fs.getSelectedFile();
            try {
                Document d = new Document();
                PdfWriter writer = PdfWriter.getInstance(d, new FileOutputStream(fi.getPath().concat(".pdf")));
                d.open();

                d.add(new Paragraph(content));
                d.close();
                writer.close();
            } catch (Exception e) {
                //
            }
        }
    }

    public static void main(String[] args) {
        // Start all Swing applications on the EDT.
        SwingUtilities.invokeLater(texteditor::new);
    }

}
