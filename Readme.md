<h1>Readme.md</h1>
### By Bradley Grapes (17325892) and Ryan Langford(17216449)

---

To execute the program simply run the main class of the **Application.java** file.

The tests are in the **texteditorTest.java** file.

To generate the PMD report run **mvn pmd:pmd** from a terminal and it will appear in the **/reports/pmd** folder.

All maven dependancies are declared in the **pom.xml** file.

<h2>Significant Commits</h2>


---
<h3>From Ryan</h3>
>**Commit a53b46a**
>
>This was the first test of Bradley being able to pull the commits I had made, and it contained the basic structure of the project

>**Commit 27d3b7f**
>
> This was where I manged to figure out that I had to edit the **pom.xml** to generate the PMD report in the correct folder.

<h3>From Bradley</h3>
>**Commit 1bd27bd**
>
> This was when I added printing function to the program.

>**Commit 17ef821**
>
> This is where I added the language syntax functionality along search function functionality with each instance of the word or part of being highlighted.

---
<h2>Extra Features</h2>
>
> The syntax selector this allows you to select the language and how the code is displayed dependant on the language selected.


